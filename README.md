# Program zaliczeniowy temat nr 7 PCA

przykładowe użycie:
```
python3 pca_zaliczenie.py --time --num_components 2 --output out.csv eeg_czas_2_128_129.csv 
```

```
argumenty: 
    input - ścieżka do pliku wejsciowego. Wymagany format csv oddzielany znakiem ;
opcje:
    --time - Jeśli pierwsza kolumna w pliku zawiera czas
    --output - ścieżka do pliku wyjsciowego, domyślnie data_out.csv
    --num_components - Ilośc komponentów wyjściowych, domyślnie tyle ile kolumn w pliku
```
