#!/usr/bin/env python3
import argparse
import pandas as pd
from matplotlib import pyplot as plot
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

parser = argparse.ArgumentParser(prog='pca_zaliczenie.py',
                                 description='pca_zaliczenie')
parser.add_argument('input',
                    help='ścieżka do pliku wejściowego. Wymagany format csv oddzielany znakiem ;')
parser.add_argument('--time',
                    action='store_true',
                    help='Czy pierwsza kolumna zawiera czas')
parser.add_argument('--output',
                    help='ścieżka do pliku wyjścowiego, domyślnie data_out.csv')
parser.add_argument('--num_components', type=int,
                    help='Ilość komponentów wyjściowych')
args = parser.parse_args()

file = pd.read_csv(args.input, sep=';', header=None)

num_samples = len(file)
sample_rate = 0
if args.time:
    sample_rate = num_samples / file.iloc[-1, 0]
    file = file.iloc[:, 1:]

columns = file.columns
num_components = args.num_components if args.num_components else len(columns)

scaler = StandardScaler()
norm_data = scaler.fit_transform(file)
norm_dataframe = pd.DataFrame(norm_data, columns=columns)
pca = PCA(num_components)
pca_transf = pca.fit_transform(norm_dataframe)
pca_dataframe = pd.DataFrame(pca_transf)

figure, axes = plot.subplots(len(columns), 2, figsize=(15, 3 * len(columns)))

for i, column in enumerate(columns):
    axes[i, 0].plot(range(len(file)), file[column], label=f'X{column + 1}')
    axes[i, 0].set_title(f'Oscylogram - Kanał {column + 1}')
    axes[i, 0].set_xlabel('próbka')

for i in range(len(pca_dataframe.columns)):
    axes[i, 1].plot(range(len(pca_dataframe)), pca_dataframe[i], label=f'PC{i + 1}')
    axes[i, 1].set_title(f'Oscylogram - PC {i + 1}')
    axes[i, 1].set_xlabel('próbka')

plot.tight_layout(rect=(0., 0., 1., 0.85))

plot.figtext(0.5, 0.95, f'Liczba próbek: {num_samples}', ha='center', fontsize=12)
if args.time:
    plot.figtext(0.5, 0.90, f'Częstotliwość próbkowania: {sample_rate} Hz', ha='center', fontsize=12)
plot.show()

pca_dataframe.to_csv(args.output if args.output else 'data_out.csv',
                     sep=';', index=False, header=False)
